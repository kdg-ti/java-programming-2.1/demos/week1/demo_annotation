package be.kdg.java2;

import be.kdg.java2.annotations.Heavy;

public class ClassWithHeavyMethods {
    @Heavy(maxTime = 500)
    public static void calculatePi(){
        System.out.println("calculating PI up to 30000 digits!");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("PI found!");
    }

    public static void printHello(){
        System.out.println("Hello!");
    }
}
